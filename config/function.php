<?php
require "config.php";
function Search($kunci, $table) {
	$query = "SELECT * FROM $table INNER JOIN tb_person USING(id_person)
    INNER JOIN tb_zakat USING(id_zakat)
    INNER JOIN tb_metode USING(id_metode) WHERE Nama LIKE '%$kunci%'";
	$result = $GLOBALS['connect']->query($query);
	return $result;
}

function tanggal($waktu) {
	return date("d F Y H:i", strtotime($waktu));
}

function getProduk() {
	$query = "SELECT * FROM tb_zakat";
    $result = $GLOBALS['connect']->query($query);
	
	while ($data = mysqli_fetch_array($result)) {
		echo "<option value=$data[Ukuran_Zakat]>$data[Nama_Zakat] @$data[Ukuran_Zakat]</option>";
	}
}
function change($value) {
	if ($value < 1000) {
		return $value . " Kg";
	} else {
		return "Rp. " . number_format($value, 0, ',', '.');
	}
}
function getzakat() {
    $query = "SELECT COUNT(*) AS Penzakat, (SELECT SUM(Jumlah_bayar) FROM tb_daftar INNER JOIN tb_Zakat USING(id_Zakat) WHERE id_zakat = 'ZB') AS ZB,
	(SELECT SUM(Jumlah_bayar) FROM tb_daftar INNER JOIN tb_Zakat USING(id_Zakat) WHERE id_zakat = 'ZU') AS ZU,
	(SELECT COUNT(*) FROM tb_confirm) AS Pendaftar  
	FROM tb_daftar INNER JOIN tb_person USING(id_person)
    INNER JOIN tb_zakat USING(id_zakat)
    INNER JOIN tb_metode USING(id_metode)";
    $result = $GLOBALS['connect']->query($query);
    $data = mysqli_fetch_assoc($result);
	return $data;
}
function CheckNull($value) {
	if ($value === NULL) {
		return 0;
	} else {
		return $value;
	}
}

function GetDataMYSQL($query) {
	$result = $GLOBALS['connect']->query($query);
	$data = mysqli_fetch_assoc($result);
	return $data;
}

function validationLogin($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

class Penyimpanan {
	private $Nama_Lengkap;
	private $No_HP;
	private $Jumlah_jiwa;
	private $Methode_Pay;
	private $Jenis_Zakat;
	private $Total_Bayar;
	private $Result;
	private $DB;

	function __construct($nama, $sapaan, $id_no, $no_hp, $jumlah_jiwa, $metode_bayar, $jenis_zakat) {
		$this->Nama_Lengkap = $sapaan . $nama;
		$this->No_HP = $id_no . $no_hp;
		$this->Jumlah_jiwa = $jumlah_jiwa;
		$this->Methode_Pay = $metode_bayar;
		$this->Jenis_Zakat = $jenis_zakat;
		$this->Total_Bayar = $jumlah_jiwa * $jenis_zakat;
	}
	function SetDB($db) {
		$this->DB = $db;
	}
	function InsertPerson() {
		$Query = "INSERT INTO tb_person VALUES(NULL, '$this->Nama_Lengkap', $this->No_HP, $this->Jumlah_jiwa)";
		$this->Result = $this->DB->query($Query);
	}
	function InsertPendaftar() {
		$Query = "INSERT INTO tb_confirm VALUES (NULL, (SELECT id_Person FROM tb_person WHERE Nama = '$this->Nama_Lengkap' LIMIT 1), 
		(SELECT Id_Zakat FROM tb_Zakat WHERE Ukuran_Zakat = '$this->Jenis_Zakat' LIMIT 1 ), 
		'$this->Methode_Pay ', $this->Total_Bayar , CURRENT_TIMESTAMP())";
		$this->Result = $this->DB->query($Query);
	}
	function getResult() {
		return $this->Result;
	}
	function CekData() {
		echo $this->Nama_Lengkap;
		echo " ";
		echo $this->No_HP;
		echo " ";
		echo $this->Jumlah_jiwa;
		echo " ";
		echo $this->Methode_Pay;
		echo " ";
		echo $this->Jenis_Zakat;
		echo " ";
		echo $this->Total_Bayar;
		echo " ";
	}

}