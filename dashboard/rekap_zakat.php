<?php
require "../config/function.php";
$cek = false;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = Search($_POST['search'], "tb_daftar");
    if (mysqli_num_rows($result) === 0) {
        $cek = true;
    }
} else {
    $query = "SELECT * FROM tb_daftar INNER JOIN tb_person USING(id_person)
    INNER JOIN tb_zakat USING(id_zakat)
    INNER JOIN tb_metode USING(id_metode)";
    $result = $connect->query($query);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Dashboard</title>
</head>

<body>
    <div class="Nav-bar">
        <div class="logo"></div>
        <div class="judul">
            <p><span class="jdl_1">Masjid Al-Muhajirin</span><span class="jdl_2">Perumahan Puri Melati, Sleman, Yogyakarta</span></p>
        </div>
    </div>
    <div class="left-panel">
        <div class="info-login">
            <img src="/ta-prpl2021-b-1900018121-aplikasi-zakat-fitrah-masjid-al-muhajirin/assets/img/user.png" alt="">
            <p>Admin</p>
        </div>
        <ul class="list-unstyled components">
            <li>
                <a href="index.php">Dashboard</a>
            </li>
            <li>
                <a aria-current="page" href="rekap_zakat.php" >Rekap Zakat</a>
            </li>
            <li>
                <a href="../Admin/">Log Out</a>
            </li>
        </ul>
    </div>
    <div class="content">
        <div class="rekap">
        <div class="search-container">
                <form action="rekap_zakat.php" method="POST">
                    <input type="text" placeholder="Search.." name="search">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <h1>Daftar Zakat Fitrah</h1>
            <table class="table">
                <thead style="background-color: #0066FF; color: white; font-weight: bold; font-size: 16px;">
                    <tr>
                        <th>No</th>
                        <th>Nama Penzakat</th>
                        <th>Kontak</th>
                        <th>Jumlah Anggota</th>
                        <th>Jenis Zakat</th>
                        <th>Metode Bayar</th>
                        <th>Jumlah Bayar</th>
                        <th>Tanggal Konfirmasi</th>
                    </tr>
                </thead>
                <tbody
                <?php if (!$cek) : ?>
                <?php $i = 1;
                    while ($data = mysqli_fetch_array($result)) : ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $data['Nama'] ?></td>
                            <td><?= $data['Kontak'] ?></td>
                            <td><?= $data['Jumlah_Person'] ?> Orang</td>
                            <td><?= $data['Nama_Zakat'] ?></td>
                            <td><?= $data['Jenis_Metode'] ?></td>
                            <td><?= change($data['Jumlah_bayar']) ?></td>
                            <td><?= tanggal($data['Tanggal']) ?></td>
                        </tr>
                    <?php endwhile; ?>
                <?php else: ?>
                <tr>
                    <td colspan="8" rowspan="2" style="font-size: 20px; font-weight: bold;">DATA TIDAK DITEMUKAN!!!</td>
                </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <footer>
        <p>Copyright @2021 Hamas. All Right Reserved</p>
    </footer>
    <script src="script/function.js"></script>
</body>

</html>