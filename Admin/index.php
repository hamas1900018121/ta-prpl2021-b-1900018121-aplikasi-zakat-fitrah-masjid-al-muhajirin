<?php 
    require "../config/config.php";
    require "../config/function.php";
    $cek = false;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Login Admin</title>
</head>

<body>
    <header class="Nav-bar">
        <div class="logo"></div>
        <div class="judul">
            <p><span class="jdl_1">Masjid Al-Muhajirin</span><span class="jdl_2">Perumahan Puri Melati, Sleman,
                    Yogyakarta</span></p>
        </div>
        <nav>
            <a href="../index.html">BERANDA</a>
            <a href="../perhitungan.php">PERHITUNGAN</a>
            <a href="../daftar.php">ZAKAT</a>
            <a href="../Admin">LOGIN</a>
        </nav>
    </header>
    <!-- FORM LOGIN -->
    <section>
        <h1 align="center">SILAHKAN LOGIN</h1>
        <h5 align="center" style="margin-bottom: 40px">Untuk Dapat Masuk ke Halaman Admin</h5>
        <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
            <div class="form">
                <label for="username">Username</label><br>
                <input id="inuser" type="text" name="username" placeholder="Masukan Username" style="width:95%;">
            </div><br>
            <div class="form">
                <label for="paDssword">Password</label><br>
                <input id="inpass" type="password" name="password" placeholder="Masukan Password" style="width: 95%;">
            </div>
            <input type="checkbox" onclick="show_pass()" id="showpass">
            <label for="showpass">Show Password</label><br>
            <?php 
            if (isset($_POST['submit'])) {
                    $username = $_POST['username'];
                    $password = $_POST['password'];
                    $query = "SELECT * FROM tb_user WHERE Username = '$username' AND Password = '$password'";
                    $result = $connect->query($query);
                    $username = validationLogin($username);
                    $password = validationLogin($password);
                    if (mysqli_num_rows($result) === 1) :  
                        header('location: ../dashboard/'); 
                    elseif ($username == "" && $password == "") :
                        echo "<h4 id='alert'>Harap Isi Username dan Password</h4>";
                    elseif ($username == "") :
                        echo "<h4 id='alert'>Harap Isi Username</h4>";
                    elseif ($password == "") :
                        echo "<h4 id='alert'>Harap Isi Password</h4>";
                    else :
                        echo "<h4 id='alert'>Username dan Password Salah!!</h4>";
                    endif; 
            } ?>
            <div class="form">
                <button class="btn-login" type="submit" name="submit">Login</button>
                <button class="btn-reset">Reset</button>
            </div>
        </form>
    </section>
    <footer>
        <p>Copyright @2021 Hamas. All Right Reserved</p>
    </footer>
    <script src="../assets/js/login.js"></script>
</body>

</html>