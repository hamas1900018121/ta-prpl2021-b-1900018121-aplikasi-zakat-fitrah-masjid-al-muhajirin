<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/perhitungan.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Perhitungan Zakat</title>
</head>
<body>
    <header class="Nav-bar">
        <div class="logo"></div>
        <div class="judul">
            <p><span class="jdl_1">Masjid Al-Muhajirin</span><span class="jdl_2">Perumahan Puri Melati, Sleman, Yogyakarta</span></p>
        </div>
        <nav>
            <a href="index.html">BERANDA</a>
            <a href="perhitungan.php">PERHITUNGAN</a>
            <a href="daftar.php">ZAKAT</a>
            <a href="Admin/">LOGIN</a>
        </nav>
    </header>
    <section class="container" id="kontainer">
		<h1 align="center" style="margin-bottom:50px;">Perhitungan Zakat</h1>
		<div class="inputan">
            <label for="pilihan_ZA"><h5><strong>Pilihan Zakat</strong></h5></label><br>
            <select name="pilihan_ZA" id="pilihan_ZA" class="form-control w-75" style="margin-bottom: 20px; font-size:18px;">
                <option value="">Pilih Jenis Zakat</option>
                <option value="2.5">Zakat Beras 2,5 Kg</option>
                <option value="30000">Zakat Uang Rp. 30.000</option>
            </select>
            <h5><strong>Jumlah Jiwa</strong></h5>
            <input type="number" min="0" class="form-control w-50" id="jum_jiwa" style="margin-bottom: 20px; font-size:18px;">
            <button class="btn btn-warning"  onclick="Menghitung();" id="btn-hitung">Menghitung</button>
        </div>
		<div style="margin-left: 580px;">
            <div>
                <h5><strong>Jenis Zakat</strong></h5>
                <input type="text" class="form-control" id="output-jenis" placeholder="Jenis">
            </div>
            <div style="text-align: left;">
                <h5 class="text-"><strong>Ukuran Zakat</strong></h5>
                <input type="text" class="form-control" id="output-ukuran" placeholder="Ukuran">
            </div>
            <div>
                <h5><strong>Jumlah Jiwa</strong></h5>
                <input type="text" class="form-control" id="output-jiwa" placeholder="Jiwa">
            </div>
            <div>
                <h5><strong>Total Perhitungan Zakat</strong></h5>
                <input type="text" class="form-control" id="output-hasil" placeholder="Total">
            </div>
		</div>
	</section>
    <footer>
        <p>Copyright @2021 Hamas. All Right Reserved</p>
    </footer>
    <script src="assets/js/function.js"></script>
</body>

</html>